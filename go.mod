module gitlab.com/gobang/bepkg

go 1.12

require (
	github.com/bradfitz/gomemcache v0.0.0-20190329173943-551aad21a668
	github.com/fastly/go-utils v0.0.0-20180712184237-d95a45783239 // indirect
	github.com/go-sql-driver/mysql v1.4.0
	github.com/go-stack/stack v1.8.0 // indirect
	github.com/golang/snappy v0.0.1 // indirect
	github.com/hashicorp/consul/api v1.1.0
	github.com/jehiah/go-strftime v0.0.0-20171201141054-1d33003b3869 // indirect
	github.com/jinzhu/copier v0.0.0-20190924061706-b57f9002281a
	github.com/jmoiron/sqlx v1.2.0
	github.com/jonboulle/clockwork v0.1.0 // indirect
	github.com/json-iterator/go v1.1.6
	github.com/labstack/echo/v4 v4.1.10
	github.com/lestrrat-go/file-rotatelogs v2.2.0+incompatible
	github.com/lestrrat-go/strftime v1.0.0 // indirect
	github.com/lib/pq v1.0.0
	github.com/mediocregopher/radix/v3 v3.4.0
	github.com/modern-go/concurrent v0.0.0-20180306012644-bacd9c7ef1dd // indirect
	github.com/modern-go/reflect2 v1.0.1 // indirect
	github.com/oklog/ulid v1.3.1
	github.com/orcaman/concurrent-map v0.0.0-20190314100340-2693aad1ed75
	github.com/pkg/errors v0.8.1
	github.com/stretchr/testify v1.4.0
	github.com/tebeka/strftime v0.1.3 // indirect
	github.com/tidwall/pretty v1.0.0 // indirect
	github.com/xdg/scram v0.0.0-20180814205039-7eeb5667e42c // indirect
	github.com/xdg/stringprep v1.0.0 // indirect
	gitlab.com/gobang/error v0.0.0-20210804155224-ac68e8e06eac
	go.mongodb.org/mongo-driver v1.1.1
	go.uber.org/atomic v1.4.0 // indirect
	go.uber.org/multierr v1.1.0 // indirect
	go.uber.org/zap v1.10.0
	google.golang.org/grpc v1.31.0
	gopkg.in/resty.v1 v1.12.0
)
